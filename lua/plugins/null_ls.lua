return {
	"jose-elias-alvarez/null-ls.nvim",
	event = { "BufReadPost", "BufNewFile" },
	config = function()
		local null_ls = require("null-ls")

		-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
		local formatting = null_ls.builtins.formatting
		-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
		-- local diagnostics = null_ls.builtins.diagnostics

		null_ls.setup {
			debug = false,
			sources = {
				-- __GOLANG>>__
--[[ GOLANG_COMMENT
				formatting.gofumpt,
GOLANG_COMMENT ]]--
				-- __<<GOLANG__
				formatting.stylua,
				-- __NODE>>__
--[[ NODE_COMMENT
				formatting.eslint,
				formatting.prettier.with {
					extra_filetypes = { "toml", "solidity" },
					extra_args = { "--no-semi", "--double-quote", "--jsx-single-quote" },
				},
NODE_COMMENT ]]--
				-- __<<NODE__
				-- __RUST>>__
--[[ RUST_COMMENT
				formatting.rustfmt,
RUST_COMMENT ]]--
				-- __<<RUST__
				-- __CXX>>__
--[[ CXX_COMMENT
				formatting.clang_format.with {
					filetypes = { "cpp", "c" },
				},
CXX_COMMENT ]]--
				-- __<<CXX__
			},
		}
	end,
}

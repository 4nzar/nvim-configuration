vim.cmd([[
language en_US.UTF-8
]])

local dok, dap = pcall(require, "dap")
if not dok then
	return
end

local mason_path = vim.fn.glob(vim.fn.stdpath "data" .. "/mason")
dap.adapters.codelldb = {
	type = "server",
	port = "${port}",
	executable = {
		command = mason_path .. "/bin/codelldb",
		args = { "--port", "${port}" },
	},
}

-- C, CPP, and Rust use codelldb.
dap.adapters.c = dap.adapters.codelldb
dap.adapters.cpp = dap.adapters.codelldb
dap.adapters.rust = dap.adapters.codelldb

-- Setup C.
dap.configurations.c = {
	{
		name = 'Launch',
		type = 'codelldb',
		request = 'launch',
		program = function ()
			-- Compile and return exec name
			local filetype = vim.bo.filetype
			local filename = vim.fn.expand("%")
			local basename = vim.fn.expand('%:t:r')
			local makefile = os.execute("(ls | grep -i makefile)")
			if makefile == "Makefile" then
				os.execute("make debug")
			else
				if filetype == "c" then
					os.execute(string.format("gcc -g -o %s %s", basename, filename))
				else
					os.execute(string.format("g++ -g -o %s %s", basename, filename))
				end
			end
			return basename
		end,
		args = function ()
			local argv = {}
			arg = vim.fn.input(string.format("argv: "))
			for a in string.gmatch(arg, "%S+") do
				table.insert(argv, a)
			end
			vim.cmd('echo ""')
			return argv
		end,
		cwd = "${workspaceFolder}",
		-- Uncomment if you want to stop at main
		-- stopAtEntry = true,
		MIMode = "lldb",
		miDebuggerPath = "/usr/bin/lldb",
		setupCommands = {
			{
				text = "-enable-pretty-printing",
				description = "enable pretty printing",
				ignoreFailures = false,
			},
		},
	},
}

-- CPP and Rust use C configurations.
dap.configurations.cpp = dap.configurations.c
dap.configurations.rust = dap.configurations.c

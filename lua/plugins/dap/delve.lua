local dok, dap = pcall(require, "dap")
if not dok then
	return
end

local mason_path = vim.fn.glob(vim.fn.stdpath "data" .. "/mason")
dap.adapters.delve = {
	name = "delve",
	type = 'server',
	port = '${port}',
	executable = {
		command = mason_path .. "/bin/dlv",
		args = {'dap', '-l', '127.0.0.1:${port}'},
	}
}

dap.configurations.go = {
	{
		type = "delve",
		name = "Debug",
		request = "launch",
		program = "${file}"
	},
	{
		type = "delve",
		name = "Debug test", -- configuration for debugging test files
		request = "launch",
		mode = "test",
		program = "${file}"
	},
	-- works with go.mod packages and sub packages 
	{
		type = "delve",
		name = "Debug test (go.mod)",
		request = "launch",
		mode = "test",
		program = "./${relativeFileDirname}"
	}
}

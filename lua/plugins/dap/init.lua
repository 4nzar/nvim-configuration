-- NOTE: Check out this for guide
-- https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation
return {
	"mfussenegger/nvim-dap",
	event = { "BufReadPost", "BufNewFile" },
	config = function()
		local dap = require("dap")
		local dapui = require("dapui")
		vim.fn.sign_define("DapBreakpoint", { text = "🛑", texthl = "DiagnosticSignError", linehl = "", numhl = "" })

		dap.listeners.after.event_initialized["dapui_config"] = function()
			dapui.open()
		end

		--[[
		dap.listeners.before.event_terminated["dapui_config"] = function()
			dapui.close()
		end

		dap.listeners.before.event_exited["dapui_config"] = function()
			dapui.close()
		end
		--]]

		-- NOTE: Make sure to install the needed files/exectubles through mason
		-- __CODELLDB>>__ 
--[[ CODELLDB_COMMENT
		require("plugins.dap.codelldb")
CODELLDB_COMMENT ]]--
		-- __<<CODELLDB__
		-- __GOLANG>>__
--[[ GOLANG_COMMENT
		require("plugins.dap.delve")
GOLANG_COMMENT ]]--
		-- __<<GOLANG__
		-- __NODE>>__
--[[ NODE_COMMENT
		require("plugins.dap.node-debug2")
NODE_COMMENT ]]--
		-- __<<NODE__
		--require "plugins.dap.node-debug2"
		--require "plugins.dap.debugpy"
		--require "plugins.dap.js-debug"
		local mappings = {
			d = {
				--				name = "Dap",
				--				c = { ":lua require'dap'.continue()<cr>", "Continue" },
				--				o = { ":lua require'dap'.step_over()<cr>", "Step Over" },
				--				i = { ":lua require'dap'.step_into()<cr>", "Step Into" },
				--				u = { ":lua require'dap'.step_out()<cr>", "Step Out" },
				--				b = { ":lua require'dap'.toggle_breakpoint()<cr>", "Breakpoint" },
				--				d = { ":lua require'dapui'.toggle()<cr>", "Dap UI" },
				--
				name = "Debug",
				R = { "<cmd>lua require('dap').run_to_cursor()<cr>", "Run to Cursor" },
				E = { "<cmd>lua require('dapui').eval(vim.fn.input '[Expression] > ')<cr>", "Evaluate Input" },
				C = { "<cmd>lua require('dap').set_breakpoint(vim.fn.input '[Condition] > ')<cr>", "Conditional Breakpoint" },
				U = { "<cmd>lua require('dapui').toggle()<cr>", "Toggle UI" },
				b = { "<cmd>lua require('dap').step_back()<cr>", "Step Back" },
				c = { "<cmd>lua require('dap').continue()<cr>", "Continue" },
				d = { "<cmd>lua require('dap').disconnect()<cr>", "Disconnect" },
				e = { "<cmd>lua require('dapui').eval()<cr>", "Evaluate" },
				g = { "<cmd>lua require('dap').session()<cr>", "Get Session" },
				h = { "<cmd>lua require('dap.ui.widgets').hover()<cr>", "Hover Variables" },
				S = { "<cmd>lua require('dap.ui.widgets').scopes()<cr>", "Scopes" },
				i = { "<cmd>lua require('dap').step_into()<cr>", "Step Into" },
				o = { "<cmd>lua require('dap').step_over()<cr>", "Step Over" },
				p = { "<cmd>lua require('dap').pause.toggle()<cr>", "Pause" },
				q = { "<cmd>lua require('dap').close()<cr>", "Quit" },
				r = { "<cmd>lua require('dap').repl.toggle()<cr>", "Toggle Repl" },
				s = { "<cmd>lua require('dap').continue()<cr>", "Start" },
				t = { "<cmd>lua require('dap').toggle_breakpoint()<cr>", "Toggle Breakpoint" },
				x = { "<cmd>lua require('dap').terminate()<cr>", "Terminate" },
				u = { "<cmd>lua require('dap').step_out()<cr>", "Step Out" },
			},
		}
		which_key_add(mappings, "n")
	end,

	dependencies = {
		{
			"rcarriga/nvim-dap-ui",
			config = function()
				require("dapui").setup {
					layouts = {
						{
							elements = {
								-- Elements can be strings or table with id and size keys.
								{ id = "scopes", size = 0.25 },
								"breakpoints",
								"stacks",
								"watches",
							},
							size = 40, -- 40 columns
							position = "left",
						},
						{
							elements = {
								"repl",
								"console",
							},
							size = 0.25, -- 25% of total lines
							position = "bottom",
						},
					},
				}
			end,
		},

		{
			"jay-babu/mason-nvim-dap.nvim",
--			dependencies = "williamboman/mason-nvim-dap.nvim",
			config = function()
				local mason = require("mason")
				local mason_dapconfig = require("mason-nvim-dap")

				local auto_install = {
					-- __<<ENCODING__
					-- __CODELLDB>>__ 
--[[ CODELLDB_COMMENT
					"codelldb",
CODELLDB_COMMENT ]]--
					-- __<<CODELLDB__
					-- __GOLANG>>__
--[[ GOLANG_COMMENT
					"delve",
GOLANG_COMMENT ]]--
					-- __<<GOLANG__
					-- __NODE>>__
--[[ NODE_COMMENT
					"node-debug2-adapter",
NODE_COMMENT ]]--
					-- __<<NODE__
				}

				mason.setup {
					ui = {
						-- Whether to automatically check for new versions when opening the :Mason window.
						check_outdated_packages_on_open = false,
						border = "single",
						icons = {
							package_pending = " ",
							package_installed = " ",
							package_uninstalled = " ",
						},
					},
				}

				mason_dapconfig.setup {
					ensure_installed = auto_install,
				}
			end,
		}
	},
}

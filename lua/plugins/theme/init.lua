return
{
	-- __START_THEME_NIGHTLY__
	{
		"Alexis12119/nightly.nvim",
		lazy = true,
		priority = 1000,
	},
	-- __END_THEME_NIGHTLY__

	-- __START_THEME_TOKYONIGHT__
	{
		"folke/tokyonight.nvim",
		lazy = true,
		priority = 1000,
	},
	-- __END_THEME_TOKYONIGHT__

	-- __START_THEME_CATPUCCIN__
	{
		"catppuccin/nvim",
		lazy = true,
		name = "catppuccin",
		priority = 1000,
	},
	-- __END_THEME_CATPUCCIN__
}
